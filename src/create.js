const path = require('path')
const fs = require('fs-extra')
const inquirer = require('inquirer')
const Creator = require('./CreatorGitlab')

module.exports = async function (appName, options) {
  // 创建项目
  const cwd = process.cwd();
  const targetDir = path.join(cwd, appName);

  if (fs.existsSync(targetDir)) {
    if (options.force) {
      await fs.remove(targetDir)
    } else {
      // 提示用户是否确定要覆盖

      // 配置询问方式
      let { action } = await inquirer.prompt([
        {
          name: 'action',
          type: 'list',
          message: `target directory already exist pick an action`,
          choices: [
            {
              name: 'Overwrite', value: 'overwrite',
            },
            {
              name:'Cancel', value: 'false'
            }
          ]
        }
      ])
      if (!action) {
        return
      } else if (action === 'overwrite') {
        console.log('\r\nRemoving...');
        await fs.remove(targetDir)
      }
    }
  }

  const creator = new Creator(appName, targetDir)
  creator.create();
}