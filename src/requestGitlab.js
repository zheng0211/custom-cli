
const axios = require('axios')

axios.interceptors.response.use(res => res.data)

async function fetchRepoList() {
  return axios.get('https://gitlab.com/api/v4/groups/9889108/projects')
}

async function fetchTagList(repo) {
  return axios.get(`https://api.github.com/repos/zhu-cli/${repo}/tags`)
}

module.exports = {
  fetchRepoList,
  fetchTagList,
}