const ora = require('ora')

async function sleep(n) {
  return new Promise((resolve) => setTimeout(resolve, n))
}

// 等待的 loading
async function wrapLoading(fn, message, ...args) {
  const spinner = ora(message)
  spinner.start() // 开始加载
  try {
    let repos = await fn(...args);
    spinner.succeed()
    return repos;
  } catch (e) {
    console.log(e);
    spinner.fail('request failed, refresh..')
    await sleep(1000)
    return wrapLoading(fn, message, ...args)
  }
}

module.exports = {
  sleep,
  wrapLoading
}