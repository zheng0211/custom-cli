const program = require('commander')
const chalk = require('chalk')
const { version } = require('./constant')
/*
  返回传了什么参数
*/
const cleanArgs = (cmd) => {
  const args = {}
  cmd.options.forEach(element => {
    const key = element.long.slice(2)
    if (cmd[key]) args[key] = cmd[key]
  });

  return args;
}


program
  .command(`create <app-name>`)
  .description(`create a new project`)
  .option('-f, --force', 'overwrite target directory if it exists')
  .action((appName, cmd) => {
    require('../src/create')(appName, cleanArgs(cmd))
  })

program
  .command(`config [value]`)
  .description(`inspect and modify the config`)
  .option('-g, --get <path>', 'get value from option')
  .option('-s, --set <path> <value>')
  .option('-d, --delete <path>', 'delete option from config')
  .action((value, cmd) => {
    console.log(value, cleanArgs(cmd));
  })

program.on('--help', function () {
  console.log();
  console.log(`Run ${chalk.cyan(`custom-vue-cli <command> --help`)} for detailed usage of given command.`)
  console.log();
})

program
  .version(`custom-vue-cli ${version}`)
  .usage(`<command> [option]`)
  .parse(process.argv)