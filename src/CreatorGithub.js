const inquirer = require('inquirer');
const { fetchRepoList, fetchTagList } = require('./requestGithub')
const { wrapLoading } = require('./util')
const downloadGitRepo = require('download-git-repo') //不支持 promise
const { promisify } = require('util');
const fs = require('fs-extra')
const path = require('path')
const templateRender = require('./templateRender')

module.exports = class Creator{
  constructor(appName, targetDir) {
    this.appName = appName
    this.targetDir = targetDir
    this.downloadGitRepo = promisify(downloadGitRepo)
  }
  async fetchRepo() {
    // 失败重新拉取

    let repos = await wrapLoading(fetchRepoList, 'waiting fetch template');

    if (!repos) return;

    repos = repos.map(item => item.name)

    let { repo } = await inquirer.prompt({
      name: 'repo',
      type: 'list',
      choices: repos,
      message: 'please choose a template to create project'
    })
    return repo;
  }
  async fetchTag(repo) {
    let tags = await wrapLoading(fetchTagList, 'waiting fetch tag', repo)
    if (!tags) return
    let { tag } = await inquirer.prompt({
      name: 'tag',
      type: 'list',
      choices: tags,
      message: 'please choose a tag to create project'
    }) 
    return tag;
  }
  async download(repo, tag) {
    // 1.拼接下载路径
    let requestUrl = `zhu-cli/${repo}${tag?'#'+tag:''}`
    // 2. 把资源下载到某个路径上 （可以增加缓存功能，应该下载到系统目录中，稍后可以再使用 ejs handlebar 去渲染模板 最后生成结果 再写入）
    const downloadDirectory = `${process.env[process.platform === 'darwin' ? 'HOME' : 'USERPROFILE']}/.template`;

    const dest = `${downloadDirectory}/${repo}`
    try {
      await fs.ensureDir(dest)
      console.log('success!')
    } catch (err) {
      console.error(err)
    }

    console.log("requestUrl:" + requestUrl);
    await this.downloadGitRepo(requestUrl, dest);
    return dest;
  }
  async copyRepo(target,appName) {
    let ncp = require('ncp'); 
    ncp = promisify(ncp);
    // 将下载的文件拷贝到当前执行命令的目录下
    await ncp(path.resolve(target, '../', 'dest', appName), path.join(path.resolve(), appName));
  }
  async create() {
     
    // 先拉取当前组织下的模板
    let repo = await this.fetchRepo()
    // 通过模板找到版本号
    let tag = await this.fetchTag(repo)

    // 下载
    let dest = await wrapLoading(this.download.bind(this), 'waiting download repo', repo, tag)
    // 根据用户的选择动态的生成内容
    try {
      await templateRender(dest, this.appName)
    } catch (e) {
      console.log(`templateRender${e}`);
    }
    // 拷贝到当前目录
    console.log('copyRepo');
    this.copyRepo(dest, this.appName)
   }
}