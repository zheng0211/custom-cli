const MetalSmith = require('metalsmith'); // 遍历文件夹
const inquirer = require('inquirer')
const path = require('path')
const fs = require('fs-extra')
const { promisify } = require('util')
// const rm = require('rimraf')
let { render } = require('consolidate').ejs;
render = promisify(render); // 包装渲染方法

module.exports = async function (target, projectName) {
      // 有ask文件说明需要编译
  if (fs.existsSync(path.join(target, 'ask.json'))) {
    console.log(target, projectName);
    await new Promise((resovle, reject) => {
      console.log(__dirname);
      MetalSmith(__dirname)
        .source(target) // 遍历下载的目录
        .destination(path.join(target, '../dest', projectName)) // 输出渲染后的结果
        .use(async (files, metal, done) => {
          // 弹框询问用户
          const result = await inquirer.prompt(require(path.join(target, 'ask.json')));
          const data = metal.metadata();
          Object.assign(data, result); // 将询问的结果放到metadata中保证在下一个中间件中可以获取到
          // console.log(files);
          delete files['ask.json'];
          done();
        })
        .use((files, metal, done) => {
          Reflect.ownKeys(files).forEach(async (file) => {
            let content = files[file].contents.toString(); // 获取文件中的内容
            if (file.includes('.js') || file.includes('.json')) { // 如果是js或者json才有可能是模板
              if (content.includes('<%=')) { // 文件中用<% 我才需要编译
                content = await render(content, metal.metadata()); // 用数据渲染模板
                files[file].contents = Buffer.from(content); // 渲染好的结果替换即可
              }
            }
          });
          done();
        })
        .build((err) => {
          // 执行中间件
          console.log(err);
          // rm('./ask.json', function (err) { // 删除当前目录下的 test.txt
          //   console.log(err);
          // });
          if (!err) {
            resovle();
          } else {
            reject();
          }
        });
    });
  }
}
